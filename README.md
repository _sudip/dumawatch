References
----------

API
===

* http://api.duma.gov.ru/pages/dokumentatsiya

* http://quantviews.blogspot.ru/search/label/%D0%B4%D1%83%D0%BC%D0%B0
* http://quantviews.blogspot.ru/2013/01/1_16.html
* http://quantviews.blogspot.ru/2013/04/2012.html

Math
====

* https://en.wikipedia.org/wiki/Multidimensional_scaling
* https://stat.ethz.ch/R-manual/R-devel/library/stats/html/dist.html
* http://stackoverflow.com/a/38205609/1133157


Development using Nix
---------------------

For Nix users, one may open development shell with all the packages installed by
typing

    $ nix-shell

Inside the shell, normal commands like `ipython` may be used. Check
`default.nix` for the details.

