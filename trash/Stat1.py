import numpy as N
import matplotlib.pyplot as P

import Load
import Cache

def assert_ndarray(x):
  assert type(x) is N.ndarray

def bernoulliPosterior_zn(theta, theta_pdf, z, n):
  assert_ndarray(theta)
  assert_ndarray(theta_pdf)

  data_given_theta = theta**z * (1-theta)**(n-z)
  theta_given_data = data_given_theta * theta_pdf
  theta_given_data_pdf = theta_given_data / sum (theta_given_data)
  return theta_given_data_pdf

def bernoulliPosterior(theta, theta_pdf, data):
  assert_ndarray(data)
  z = len([x for x in data if x == True])
  n = len(data)
  return bernoulliPosterior_zn(theta,theta_pdf, z, n)

def HDI_of_grid(probMassVec, credMass=0.95):
  sortedProbMass = N.sort(probMassVec, axis=None)[::-1]
  HDIheightIdx = N.min(N.where(N.cumsum(sortedProbMass) >= credMass))
  HDIheight = sortedProbMass[HDIheightIdx]
  HDImass = N.sum(probMassVec[probMassVec >= HDIheight])
  idx = N.where(probMassVec >= HDIheight)
  return {'indices':idx[0], 'mass':HDImass, 'height':HDIheight}


'''
n = 1000
theta = N.linspace(1/n,1-1/n,n)
theta_y = N.minimum(theta , 1-theta)
theta_pdf = theta_y / sum(theta_y)
prior = theta_pdf

data = N.array(Cache.votes_er)[1:1530]
z = len([x for x in data if x == True])
n = len(data)


poster = bernoulliPosterior(theta, theta_pdf, data)
'''

n = 1000
theta = N.linspace(1/n,1-1/n,n)
theta_y = N.array([1.0 for x in theta])
theta_pdf = theta_y / sum(theta_y)
prior = theta_pdf

# poster = bernoulliPosterior_zn(theta, theta_pdf, 58+57, 100+100)
# hdi = HDI_of_grid(poster)

# bar0 = (theta[hdi['indices'][0]], theta[hdi['indices'][0]])
# bar1 = (0, 0.01)

# bar2 = (theta[hdi['indices'][-1]], theta[hdi['indices'][-1]])
# bar3 = (0, 0.01)


data = N.array(Cache.votes_er)[1:1530]
z = len([x for x in data if x == True])
n = len(data)
print("z=%d N=%d" % (z,n))
data_given_theta = theta**z * (1-theta)**(n-z)
data_given_theta_pdf = data_given_theta / sum(data_given_theta)
data_p = sum(data_given_theta * theta_pdf)
theta_given_data_pdf = (data_given_theta * theta_pdf) / data_p
poster = theta_given_data_pdf

# P.plot(theta, prior, 'g-', theta, poster, 'b-')
P.plot(theta, prior, 'g-', theta, poster, 'b-')
P.show()



# def testPosterior():
#   # bernoulliPosterior(N.array([]), N.array([]), N.array(Cache.votes_er))
#   # tgd = bernoulliPosterior(theta_x, theta_y, N.array(Cache.votes_er))
#   P.plot(theta_x, prior)
#   P.plot(theta_x, poster)
#   P.show()


def countVotes():
  x = Cache.votes_er
  f=0
  a=0
  for v in x:
    if v: f=f+1
    else: a=a+1
  print("For %d, Against: %d, Total: %d"
         % (f,a,len(x)))
