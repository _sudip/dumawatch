{ pkgs ?  import <nixpkgs> {}
, stdenv ? pkgs.stdenv
} :
let

  pydev = stdenv.mkDerivation {
    name = "pydev";
    buildInputs =
      with pkgs;
      with pkgs.python3Packages;
    [
      ipython
      python
      scipy
      numpy
      matplotlib
      xlibs.xeyes
      pycairo
      pyqt5
      pygobject3
      gtk3
      gobjectIntrospection
      requests2
      pandas
      ctags
      scikitlearn
    ];

    shellHook = ''
      export PYTHONPATH="`pwd`/src:$PYTHONPATH"
      export MPLBACKEND='Qt5Agg'
      alias ipython='ipython --matplotlib=qt5'

      echo Building ctags...
      find $buildInputs ./src -name '*\.py' | grep -v '/tests/' | xargs ctags
      echo Done.
    '';
  };

in
  pydev
