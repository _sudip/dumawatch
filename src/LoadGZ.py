import requests
import json
import gzip
import os
import sys
import traceback

import pandas as pd
import numpy as np

class Error(Exception):
  pass

def api_raw(method, args=[]):
  ''' Perform API call to the Duma API, return JSON object containing the result
      See also http://api.duma.gov.ru/pages/dokumentatsiya
  '''
  api_key = "daae6084141c6568ba3682382bd6a444aafff3c4"
  app_key = "app731f9a7187f38844b681a0139afe60da696f6b47"
  url = "http://api.duma.gov.ru/api/%s/%s.json?%s" % \
            (api_key, method, '&'.join(['app_token=%s' % app_key] + [x for x in args if x != '']))
  # proxyDict = { "socks" : "socks5://127.0.0.1:4343" }
  proxyDict = None
  r = requests.get(url, proxies=proxyDict)
  if r.status_code != 200:
    print('Status', r.status_code)
    print(r.json())
    raise Error(url)
  return r.json()

def api_test():
  test = api_raw("vote/95889")
  print(test)
  for d in test["resultsByDeputy"]:
    print(d)

def fetchVotes(filename='data/vote.json.gz', limit=100, dfrom='', dto=''):
  ''' Fetch brief vote information from API server to disk.
  dfrom, dto are start and stop dates '''
  tmp = filename+'.tmp'
  if dfrom != '':
    dfrom = 'from=%s' % dfrom
  if dto != '':
    dto = 'to=%s' % dto

  def wrt(data):
    fnm = filename
    tmp = fnm + '.tmp'
    with gzip.open(tmp, 'wt') as f:
      json.dump(data, f, indent=4, ensure_ascii=False)
    os.rename(tmp, fnm)

  i=1
  jslist=[]
  while True:
    print ('Fetching to', filename, 'page =',i)
    js = api_raw("voteSearch", ['page=%d'%i, 'sort=date_asc', 'limit=100', dfrom, dto])
    if ('code' in js) and (js['code'] == 5):
      print(js)
      print('Retrying')
      continue
    if ('totalCount' not in js) or (len(jslist)>=int(js['totalCount'])):
      print(js)
      print('Done fetching. Fetched', len(jslist) , 'vote records')
      break
    jslist.extend(js['votes'])
    if(i%10 == 0):
      wrt(jslist)
    i=i+1
  wrt(jslist)



def fetchVoteDetails(voteContnents='data/vote.json.gz', voteDetails='data/duma/details-%d.json.gz'):
  ''' Fetch detailed vote information from server to json.gz archive.
  voteContents is a result of fetchVotes. voteDetails is a masked filename to be
  used to write information. '''
  def rd(sect):
    try:
      with gzip.open(voteDetails % sect, 'rb') as f:
        return json.loads(f.read().decode('utf-8-sig'))
    except:
      print("Warning, no read for chunk %d" % sect)
      return []

  def wrt(details, sect):
    fnm = voteDetails % sect
    tmp = fnm + '.tmp'
    with gzip.open(tmp, 'wt') as f:
      json.dump(details, f, indent=4, ensure_ascii=False)
    os.rename(tmp, fnm)

  votes_all = loadVotes(voteContnents)

  chunk_sz = round(len(votes_all)/25)
  votes_chunked = [votes_all[i:i + chunk_sz] for i in range(0, len(votes_all), chunk_sz)]

  for nchunk, votes in enumerate(votes_chunked):
    details = rd(nchunk)
    votes2 = [x for x in votes if not ( x["id"] in [i["id"] for i in details])]
    print("Chunk %d, Going to fetch %d votes, Cleared out %d existing elements"
      % (nchunk, len(votes2), len(votes) - len(votes2)))

    for v in votes2:
      while True:
        print("Chunk %d of %d, Vote %d of %d"
                % (nchunk, len(votes_chunked), len(details), len(votes)))
        id = v["id"]
        js = api_raw("vote/%d" % id)
        if ('code' in js) and (js['code'] == 5):
          print(js)
          print('Retrying vote/%d' % id)
          continue
        js["id"] = id
        details.append(js)
        if len(details) % 10 == 0 :
          wrt(details, nchunk)
        break

    wrt(details, nchunk)

def loadVotes(voteSearch='data/vote.json.gz'):
  ''' Load brief vote information from disk into memory, return JSON object '''
  f = gzip.open(voteSearch, 'rb')
  c = f.read().decode('utf-8-sig')
  return json.loads(c)


def loadVoteDetails(voteDetails='data/duma/details-%d.json.gz', handler = lambda x:x):
  '''  Loads detailed vote information from (one or more) GZ archive into memory. Return JSON
  object.  Handler is called for every GZ archive being processed.'''
  sect = 0
  read1 = False
  while True:
    try:
      with gzip.open(voteDetails % sect, 'rb') as f:
        votes = json.loads(f.read().decode('utf-8-sig'))
        read1 = True
        cont = handler(votes)
        if not cont:
          return
      sect = sect+1
    except FileNotFoundError as e:
      if not read1:
        raise
      else:
        return

def loadVoteDetailsFaction(voteDetails='data/duma/details-%d.json.gz'):
  '''  Loads vote information from (one or more) GZ archive into memory, but don't load individual
  votes of deputies. Return JSON object '''
  r = []
  def readFaction(vs):
    nonlocal r
    for v in vs:
      if "resultsByDeputy" in v:
        del v["resultsByDeputy"]
      r.append(v)
    return True
  loadVoteDetails(voteDetails, readFaction)
  return r


class Error(Exception):
  pass

def enn(cont, fld):
  ''' Helper function: enn stands for "Exists and Non-None"'''
  return fld in cont and (not cont[fld] is None)

def votesdf(vs):
  ''' Create blank deputi-vote dataframe (columns and index are set) '''
  vote_ids = []
  for v in vs:
    if enn(v,'id'):
      vote_ids.append(int(v['id']))

  deputies_ids = set()
  for v in vs:
    if 'resultsByDeputy' in v:
      for r in v['resultsByDeputy']:
        if enn(r,'code'):
          deputies_ids.add(int(r['code']))

  return pd.DataFrame(
    columns = [ list(deputies_ids) ],
    index   = [ vote_ids ]
    )


def loadDeputyVoteDF(voteDetails='data/duma/details-%d.json.gz'):
  ''' Loads detailed voting information into a dataframe called Deputy-Vote
  Frame. Indexes - deputy codes, Columns - vote identifiers. The data cells are
  filled with voting decisions, taken by specific deputy during specific vote.
  See deputyResult for details.
  '''

  def deputyResult(rd):
    x = rd['result']
    if x is None:
      return np.nan
    if x == "for":
      return 1
    elif x == "against":
      return 0
    elif x == "absent":
      return -1
    elif x == "abstain":
      return -1
    else:
      raise Error ("Invalid voting result '%s' for '%s'" % (x,rd))

  deputies = pd.DataFrame()

  def readDeputyVotes(vs):
    nonlocal deputies

    d = votesdf(vs)

    for v in vs:
      row = []
      if 'resultsByDeputy' in v:
        for rd in v['resultsByDeputy']:
          if enn(rd, 'code') and 'result' in rd:
            row.append( (int(rd['code']), deputyResult(rd)) )
      if enn(v,'id'):
        d.loc[ int(v['id']) ] = dict(row)

    deputies = deputies.append(d)
    return True

  loadVoteDetails(voteDetails, readDeputyVotes)
  return deputies.T.sort_index()


def loadDeputyFactionDF(voteDetails='data/duma/details-%d.json.gz'):
  ''' Loads detailed voting information into a dataframe called Deputy-Faction
  Frame where Indexes are deputy codes, Columns are vote identifiers. The data cells are
  filled with faction codes of scecific deputy during specific voting.
  '''

  df = pd.DataFrame()
  def rd(vs):
    nonlocal df

    d = votesdf(vs)

    for v in vs:
      row = []
      if 'resultsByDeputy' in v:
        for rd in v['resultsByDeputy']:
          if enn(rd, 'code') and enn(rd,'faction'):
            row.append( (int(rd['code']), rd['faction']) )
      if enn(v,'id'):
        d.loc[ int(v['id']) ] = dict(row)

    df = df.append(d)
    return True

  loadVoteDetails(voteDetails, rd)
  return df.T.sort_index()

