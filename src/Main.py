import json
import gzip
import os
import LoadGZ as Load
import Deputy1
import LoadDB as db
import pandas as pd
import sqlite3


contents = './data/vote7.json.gz'
details = './data/duma7/details-%d.json.gz'

def fetch():
  ''' Fetch brief  voting information to the local storage '''
  fetchVotes(contents, dfrom='2016-10-05')

def fetchDetails():
  ''' Using brief voting information, fetch detailed voting information to the
  local storage '''
  fetchVoteDetails(contents, details)

dvf = None
def loadDVF():
  global dvf
  dvf = Load.loadDeputyVoteDF(details)

dff = None
def loadDFF():
  global dff
  dff = Load.loadDeputyFactionDF(details)

def plot():
  Deputy1.plot(dff, Deputy1.calculate(dvf))



def main():
  ''' Load both dataframes from local storage and plot the result '''
  os.chdir('..')
  print("Loading DVF")
  loadDVF()	
  print(dvf)
  input()
  print("Loading DFF")
  loadDFF()
  print(dff)
  print("Plotting")
  
def __main():
	global dff,dvf
	db.__checkSQLFile()
	db.fetchVotesDB('db.sqlite3',dfrom='2016-01-01',)# fetch only 10 votes.
	db.fetchDetailsDB('db.sqlite3',limit=20)# fetch details of only first 10 votes in Votes table.

	dvf,dff=db.loadDeputyVote()
	dff.T.sort_index()
	dvf.T.sort_index()
	
	print(dvf)
	print(dff)
	plot()
__main()
