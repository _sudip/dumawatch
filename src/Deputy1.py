import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy.spatial.distance import pdist, squareform
from sklearn.manifold import MDS

import LoadGZ as  Load

def calculate(dvf):
  ''' Take deputy-vote frame, calculate MDS coords '''
  dist = squareform(pdist(dvf.fillna(-1).values))
  mds = MDS(random_state = 54)
  coords = mds.fit_transform(dist)
  return coords

def plot(dff, coords):
  ''' Take deputy-faction frame and coords obtained by 'calculate', after that
  draw a figure'''
  print(coords.shape, dff.shape)
  assert coords.shape[0] == dff.shape[0], "number of rows should be equal"

  fact = dff.apply(lambda x: x.dropna().mode(), axis=1)
  colormap = dict(zip(list(set(fact[0].values)), ['purple', 'yellow','black', 'orange', 'blue', 'red']))
  print(colormap)

  def color(faction):
    return colormap[faction]

  f = plt.figure(1)
  ax = plt.axes([0.,0.,1.,1.])
  plt.scatter(coords[:,0], coords[:,1], c=[color(x) for x in fact[0].values])
  plt.show()

