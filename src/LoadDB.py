#!/usr/bin/python3  
import sqlite3
import requests
import json
import sys
import pandas
import numpy

def __checkSQLFile(filename='db.sqlite3'):
  connection=sqlite3.connect(filename)
  c=connection.cursor()
  #All the strings in the table are TEXT instead of VARCHAR because sqlite considers VARCHARS as TEXT internally
  #create deputy table
  c.execute('''CREATE TABLE IF NOT EXISTS Deputy (
                DeputyCode	INTEGER PRIMARY KEY,
                Name	TEXT,
                Family TEXT,
                Patronymic TEXT
                )
            ''')
  #create  table Vote
  c.execute('''CREATE TABLE IF NOT EXISTS Vote (
                VoteId INTEGER PRIMARY KEY,
                LawNumber INTEGER,
                Date DATETIME,
                Subject TEXT,
                Result TEXT,
                ResultType Text,
                TranscriptLink TEXT,
                Fetched TINYINT DEFAULT(0) /*0,1 for false or true values.Denotes whether details have already been fetched or not other wise duplicate entry may be entered in the DeputyVote Table..*/
                )
            ''')
  #create table for DeputyVote to store results by deputy
  c.execute('''CREATE TABLE IF NOT EXISTS DeputyVote(
                DeputyCode INTEGER,
                VoteId INTEGER,
                FactionCode TINYINT,
                Result TEXT,
                Primary KEY (VoteId, DeputyCode) /*Entry with both of these values same cannot be repeated.*/
                )
            ''')
  # create table for faction.
  c.execute('''CREATE TABLE IF NOT EXISTS Faction(
                FactionCode TINYINT PRIMARY KEY,
                Name  TEXT,
                Abbr TEXT)
            ''')
  # create table for factionVote to store results by faction
  connection.commit()
  connection.close()
__connection=None
def __initiateSQL(filename='db.sqlite3'):
  global __connection
  if __connection is None:
    __connection=sqlite3.connect(filename)
  return __connection

def __endSQL():
  global __connection
  if __connection is not None:
    __connection.commit()
    __connection.close()
    __connection=None
  
  
  
#def fetchVotes(limit=100)

def api_raw(method, args=[]):
  ''' Perform API call to the Duma API, return JSON object containing the result
      See also http://api.duma.gov.ru/pages/dokumentatsiya
  '''
  api_key = "daae6084141c6568ba3682382bd6a444aafff3c4"
  app_key = "app731f9a7187f38844b681a0139afe60da696f6b47"
  url = "http://api.duma.gov.ru/api/%s/%s.json?%s" % \
            (api_key, method, '&'.join(['app_token=%s' % app_key] + [x for x in args if x != '']))
  # proxyDict = { "socks" : "socks5://127.0.0.1:4343" }
  proxyDict = None
  r = requests.get(url, proxies=proxyDict)
  if r.status_code != 200:
    print('Status', r.status_code)
    print(r.json())
    raise Error(url)
  return r.json()

def fetchVotesDB(filename='db.sqlite3', limit=None, dfrom='', dto=''):
  ''' Fetch brief vote information from API server to disk.
  dfrom, dto are start and stop dates 
  '''
  connection=__initiateSQL().cursor()
  if dfrom != '':
    dfrom = 'from=%s' % dfrom
  if dto != '':
    dto = 'to=%s' % dto
  i=1
  jslist=[]
  entries=0
  
  if limit is None:
    limit_str='limit=100'
  else:
    limit_str='limit=%d'%limit
    
  print('Fetching to :',filename)
  while True:
    print ('\tpage =',i)
    js = api_raw("voteSearch", ['page=%d'%i, 'sort=date_asc', limit_str, dfrom, dto])
    entries+=len(js['votes'])
    if ('code' in js) and (js['code'] == 5):
      print(js)
      print('Error code 5 : Retrying')
      continue
    try:
      connection.executemany("""INSERT OR IGNORE INTO Vote (VoteId,Date,Subject,ResultType,Result)
                              VALUES(:id,:voteDate,:subject,:resultType,:result)""",js['votes'])
    except (sqlite3.IntegrityError,sqlite3.ProgrammingError) as error:
      print("\n\nException:\t\t Inserting into Vote table Failed\n\t\t\t Data from server doesn't have all the required fields.")
      __endSQL()
      sys.exit()
    if limit is None and ('totalCount' in js) and entries<int(js['totalCount']):
      i+=1
      continue
    print("Fetch Complete : Fetched",entries,"Votes")
    __endSQL()
    break
    
    
def fetchDetailsDB(filename='db.sqlite3',voteditails=None,limit=10):
  '''Fetches details of votes that are entered in the given file.
     The details are loaded into the same file.
     Fetch details of only first (limit) votes in Vote table.
  '''
  connection=__initiateSQL().cursor()
  if limit is None:
    result=connection.execute('SELECT VoteId FROM Vote where Fetched=0')
  else :
    result=connection.execute('SELECT VoteId FROM Vote where Fetched=0 LIMIT '+str(limit))
  result=list(result)
  result=[x[0] for x in result]
  if len(result) is 0:
    print("All the vote details have already been loaded");
    return
  
  print("Loading details from server for %d Votes"%len(result));
  
  for _id in result : 
    if _id is None:
      continue;
    #print info about the fetch
    print("Fetching details for VoteId ",_id,':',end='')
    sys.stdout.flush()
    
    #fetch the vote detail from the server
    js=None
    while True:
      js = api_raw("vote/%d" % _id)
      if ('code' in js) and (js['code'] == 5):
        print(js)
        print('Retrying vote/%d' % _id)
        continue
      break
    print("\n\tData Fetched  Loading into sqlite file : ",end='')
    connection.executemany("INSERT OR IGNORE INTO Faction VALUES(:code,:name,:abbr)",js['resultsByFaction'])
    # add all the entry in 'resultsByDeputy' into the database
    errors=[]
    for entry in js['resultsByDeputy']:
      if entry.get('code') is not None:
        try:
          connection.execute("INSERT INTO DeputyVote(DeputyCode, VoteId,Result,FactionCode) values("+str(entry['code'])+','+str(_id)+',"'+str(entry['result'])+'",'+str(entry['factionCode'])+')' )
        except  Exception :
          print("Exception for Inserting DeputyCode :",str(Exception))
        query="INSERT OR IGNORE INTO Deputy(DeputyCode, Name,Family,Patronymic) VALUES("+\
                      str(entry['code'])+','+\
                      (('"'+entry['name']+'"') if entry.get('name') is not None else 'NULL')+','+\
                      (('"'+entry['family']+'"') if entry.get('family') is not None else 'NULL')+','+\
                      (('"'+entry['patronymic']+'"') if entry.get('patronymic')is not None else 'NULL')+')'
        connection.execute(query)
      else:
          errors.append(entry)
    if len(errors) >0:
      print ("Unexpected Error!\n ","Following Fetched details had no proper formatting:")
      for a in errors:
        print(a)
    print("Loaded",len(js['resultsByDeputy']),"Deputies")
        # initially vote table doesn't have all the informations. Some information is fetched in the detail so update the table
    
    print("Current value _id",_id,js['lawNumber'])
    query="UPDATE Vote SET `LawNumber`="+\
              (str(js['lawNumber']) if js.get('lawNumber') is not None else 'NULL')+\
              ',`TranscriptLink`='+(("'"+js['transcriptLink']+"'") if js.get('transcriptLink') is not None else 'NULL')+\
              ",Fetched=1 WHERE `VoteId`="; 
    query+=str(_id);
    connection.execute(query)
    
    __initiateSQL().commit()
  __endSQL()  

def loadDeputyVote(filename='db.sqlite3'):
  ''' Loads detailed voting information into a dataframe called Deputy-Vote
  Frame. Indexes - deputy codes, Columns - vote identifiers. The data cells are
  filled with voting decisions, taken by specific deputy during specific vote.
  See deputyResult for details.
  '''
  
  legendary_query='''
        SELECT DV.DeputyCode, DV.VoteId, F.Abbr AS Faction,
          CASE 
                  WHEN `Result`='absent'	  THEN -1
                  WHEN `Result`='for'		    THEN 1
                  WHEN `Result`='abstain' 	THEN -1
                  WHEN `Result`='against' 	THEN 0  
                  ELSE NULL                   
          END AS `Result`
        FROM DeputyVote AS DV
        INNER JOIN Faction AS F ON F.FactionCode=DV.FactionCode
  '''
  # legendary query  on execution gives result of following type of table:
        #
        #          DeputyCode  VoteId      Faction  Result
        #
        #  0        99111774   97009         ЛДПР      -1
        #  1        99112936   97009           ЕР       1
        #  2        99111987   97009         КПРФ       1
        #  3        99112712   97009           ЕР       1
        #  4        99109653   97009           ЕР       1
        #  5        99100784   97009           СР       1
        #  6        99111778   97009           ЕР       1
  
  table=pandas.read_sql_query(legendary_query,__initiateSQL())
  
  result_by_faction=table.pivot_table(index='DeputyCode',columns='VoteId',values='Faction',aggfunc='first')
  #replace all None with blank string.
  result_by_faction.fillna(value='', inplace=True)
  
  result_by_deputy=table.pivot_table(index='DeputyCode',columns='VoteId',values='Result',aggfunc='first')
  #replace all None with nan.
  result_by_deputy.fillna(value=numpy.nan,inplace=True)
  
  
  #First return value is for result and second for faction Abbrevation
  return result_by_deputy, result_by_faction
  
