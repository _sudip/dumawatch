SUMMARY
-------

The application basically does two things:

1. Fetches voting information from the remote server to local storage using
   available JSON API calls. Local storage are currently implemented as a
   collection of JSON.GZ archives under the 'data' folder.

2. Loads the data from local storage into memory and perfoms some
   calculations/plotting.

THE TASK
--------

The task is to implement new local storage module 'LoadDB.py'. It should mirror
exisiting LoadGZ module, but use SQLite database as backend.  The public
interface should be the same with the interface of the existing 'LoadGZ.py'
module.  Specifically, the following functions should be implemented:

* LoadDB.fetchVotes(dbstring, dfrom, dto) - fetch brief information from server
  to local storage
* LoadDB.fetchVoteDetails(dbstring) - fetch detailed information using already
  fetched brief information from server to local storage
* LoadDB.loadDeputyVoteDF(dbstring) - read the local storage and load Deputy-Vote information into
  pandas.DataFrame in memory
* LoadDB.loadDeputyFactionDF(dbstring) - read the local storage load Deputy-Faction information into
  pandas.DataFrame in memory

Internally, the local storage should be organized as a SQLite database containig the
following tables with columns of appropriate types:

* table Deputy(DeputyCode, Name, etc)
* table Vote(VoteId, lawNumber, date, transcriptLink)
* table DeputyVote(Id, DeputyCode, VoteId, Faction) - many-to-many link between
  votes and deputies
